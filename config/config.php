<?php

define("DEFAULT_ENCODE",	"UTF-8");			// 扱う文字コード

// php.iniの設定をちょっと変えます
// Ver 5.6 以降はmbstring.internal_encodingの設定は非推奨です！dd
if(version_compare(PHP_VERSION, '5.6.0') >= 0) {
    ini_set('default_charset', DEFAULT_ENCODE);
}else{
    ini_set('mbstring.internal_encoding', DEFAULT_ENCODE);
}

date_default_timezone_set('Asia/Tokyo');

/* =====================================
// DataBase定義
=====================================*/
define("DB_TYPE",	"mysql");						    // 使用データベース
define("DB_HOST",	"localhost");					    // データベースホスト
define("DB_NAME",	"vagrant-centos7-dev");     	    // データベース名
define("DB_USER",	"vagrant-centos7-dev");		               // データベースユーザ
define("DB_PASS",	"vagrant");				           // データベースユーザパスワード

/* ====================================
// 環境定義
=====================================*/
define("DEBUG_MODE",	1);							    // デバッグモード（1:プレビュー／0:本番）
//define("DEBUG_MODE",	0);							    // デバッグモード（1:プレビュー／0:本番）

define("SERVER_DOMAIN_NAME",	"pechatdl-test.preview.i-studio.co.jp");					// ドメイン名

define("SESSION_GC_MAX_LIFE_TIME",		7200);		    // セッションのGC時間

/* ====================================
// Directory定義
=====================================*/
define("DIR_ROOT",			    "/var/www/vagrant-centos7-dev/");		    // ルートディレクトリ
define("DIR_CONFIG",		    DIR_ROOT."config/");		        // コンフィグディレクトリ
define("DIR_DATA",			    DIR_ROOT."data/");			        // データディレクトリ
define("DIR_LIB",			    DIR_ROOT."lib/");			        // ライブラリディレクトリ
define("DIR_HTDOCS",		    DIR_ROOT."htdocs/");		        // ドキュメントルートディレクトリ
define("DIR_TEMPLATE",		    DIR_ROOT."template/");		        // テンプレートディレクトリ
define("DIR_LIB_PEAR",		    DIR_LIB."pear/");			        // Pearディレクトリ
define("DIR_LIB_COMMON",	    DIR_LIB."common/");		            // 共通ライブラリディレクトリ
define("DIR_LIB_FUNCTION",	    DIR_LIB."function/");	            // 個別ライブラリディレクトリ
define("DIR_LIB_OTHER",		    DIR_LIB."other/");	                // その他ライブラリディレクトリ
define("DIR_XML",			    DIR_HTDOCS."xml/");	                // XMLディレクトリ

define("DIR_PHP_LOG",		    DIR_ROOT."phplog/");			    // ログの出力ディレクトリ
define("DIR_LOG_ERROR",		    DIR_PHP_LOG."log_error/");		    // エラーログの出力ディレクトリ
define("DIR_LOG_MESSAGE",	    DIR_PHP_LOG."log_message/");	    // メッセージの出力ディレクトリ
define("DIR_LOG_BENCHMARK",     DIR_PHP_LOG."log_benchmark/");	    // ベンチマークの出力ディレクトリ

//	define('DIR_VIEWADMIN', 	DIR_ROOT . "viewadmin/");			// viewadmin

// WindowsとUNIXだとパスのセパレータが違うので、ここで判別
if (!defined('PATH_SEPARATOR')) {
    //		define('PATH_SEPARATOR', preg_match('/^windows/i', $_SERVER['OS']) ? ';' : ':');
    define('PATH_SEPARATOR', ':');		// $_SERVER['OS']は、もうないんやで
}

// インクルードパス(requireするときのファイル検索パス)を再設定
//	ini_set('include_path', CONF_DIR .PATH_SEPARATOR. DIR_LIB .PATH_SEPARATOR. DIR_LIB_PEAR .PATH_SEPARATOR. DIR_LIB_COMMON .PATH_SEPARATOR. ini_get('include_path'));
ini_set('include_path', DIR_LIB .PATH_SEPARATOR. DIR_LIB_COMMON .PATH_SEPARATOR. DIR_LIB_PEAR .PATH_SEPARATOR. DIR_LIB_OTHER .PATH_SEPARATOR. ini_get('include_path'));

//-----------------------------------------------
// URL
//-----------------------------------------------
define('URL_ROOT', (empty($_SERVER['HTTPS'])?"http://":"https://").$_SERVER['SERVER_NAME']."/");

//-----------------------------------------------
// MAIL
//-----------------------------------------------
//define("MAIL_HOST",		"localhost");			// SMTPサーバー